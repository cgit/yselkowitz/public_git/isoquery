Name:           isoquery
Version:        3.2.6
Release:        1%{?dist}
Summary:        Search and display ISO codes

License:        GPLv3+
URL:            https://github.com/toddy15/isoquery
Source0:        https://github.com/toddy15/isoquery/archive/v%{version}/%{name}-v%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  gettext-devel
BuildRequires:  glib2-devel
BuildRequires:  json-glib-devel
BuildRequires:  make
BuildRequires:  po4a
%if 0%{?rhel} == 7
BuildRequires:  python-docutils
%else
BuildRequires:  python3-docutils
%endif
Requires:       iso-codes

%description
Isoquery can be used to generate a tabular output of the ISO standard codes
provided by the package iso-codes. It parses the JSON files and shows all
included ISO codes or just matching entries, if specified on the command line.
Moreover, it's possible to get all available translations for the ISO standard.


%prep
%autosetup


%build
%configure
%make_build


%install
%make_install

%find_lang %{name} --with-man


%files -f %{name}.lang
%license COPYING
%doc AUTHORS CHANGELOG.md README.md
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1*



%changelog
* Tue Nov 30 2021 Yaakov Selkowitz <yselkowi@redhat.com> - 3.2.6-1
- new version

* Mon Aug  3 2020 Yaakov Selkowitz <yselkowi@redhat.com>
- 
